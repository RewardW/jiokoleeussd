package com.equitel.jiokolee;

import com.equitel.jiokolee.services.SessionManagement;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.statemachine.StateMachine;

@SpringBootApplication
public class JiokoleeUssdApplication{
    
        //Object to store session parameters    
        public static SessionManagement sessionMgr;
	
	@Autowired
	private StateMachine<States,Events> stateMachine;

	public static void main(String[] args) {
		SpringApplication.run(JiokoleeUssdApplication.class, args);
                
                sessionMgr = new SessionManagement();
	}
	
	public enum States {
	    INITIAL,UNQUALIFIED, MENU, CONFIRM, SUCCESS,HELP,HELP_QUALIFICATION,HELP_REPAYMENT
	}

	public enum Events {
	    CHECK_QUALIFICATION, SELECT_OPTION, CONFRIM_OPTION, QUALIFIED
	}
        /*
	@Override
	public void run(String... args) throws Exception {
		// TODO Auto-generated method stub
		stateMachine.start();
		stateMachine.sendEvent(Events.CHECK_QUALIFICATION);
		stateMachine.sendEvent(Events.QUALIFIED);
	    stateMachine.sendEvent(Events.SELECT_OPTION);
	    stateMachine.sendEvent(Events.CONFRIM_OPTION);
	    stateMachine.stop();
	}*/
}
