/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.equitel.jiokolee.utils;

import com.equitel.jiokolee.JiokoleeUssdApplication;
import com.equitel.jiokolee.pojos.UssdRequest;
import com.equitel.jiokolee.services.SessionManagement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.IOException;
import java.io.StringReader;
import java.util.Hashtable;
import java.util.Map;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;
import org.w3c.dom.DOMException;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

/**
 *
 * @author Wambayi
 */

@Service
public class UssdUtils {
    @Autowired
    Environment env;
    
    static Log log = LogFactory.getLog(UssdUtils.class.getName());
    
    public UssdRequest parseUssdReq(String req){
        
        InputSource source = new InputSource(new StringReader(req.toString()));
        
        UssdRequest newReq = new UssdRequest();
        
        try{

            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder dBuilder = dbFactory.newDocumentBuilder();
            Document doc = dBuilder.parse(source);
            doc.getDocumentElement().normalize();

            NodeList nList = doc.getElementsByTagName("USSDDynMenuRequest");
            Element eElement = (Element) nList.item(0);
            
            newReq.setRequestId(eElement.getElementsByTagName("requestId").item(0).getTextContent());
            newReq.setMsisdn(eElement.getElementsByTagName("msisdn").item(0).getTextContent());
            newReq.setTimestamp(eElement.getElementsByTagName("timeStamp").item(0).getTextContent());
            newReq.setStarCode(eElement.getElementsByTagName("starCode").item(0).getTextContent());
            
            XPath xPath = XPathFactory.newInstance().newXPath();
            String ussdParamTag = "/USSDDynMenuRequest/dataSet/param";
            NodeList paramsNode = (NodeList) xPath.compile(ussdParamTag).evaluate(doc, XPathConstants.NODESET);
                        
            System.out.println(paramsNode.getLength());
            log.info("ListCount:"+paramsNode.getLength());
            
            Map<String,String> ussdParams = new Hashtable();
            
            for(int n = paramsNode.getLength() -1; n >= 0; n--){
                
                Element option = (Element) paramsNode.item(n);
                //Put the values in a hash map
                String id = option.getElementsByTagName("id").item(0).getTextContent();
                String value = option.getElementsByTagName("value").item(0).getTextContent();
                ussdParams.put(id , value);
            }
            
            newReq.setInput(ussdParams.get("TRAVERSAL-PATH"));
            
        }catch(IOException | ParserConfigurationException | XPathExpressionException | DOMException | SAXException e){
            log.error("parseUSSD: "+e);
            newReq.setMessage("Please try again later.");
            
        }
        return newReq;
    }
    
    public String ussdResponse(UssdRequest resp){
        
        SessionManagement sessionMgt = JiokoleeUssdApplication.sessionMgr;
        
        JsonParser parser = new JsonParser();
        JsonObject jsonObj = (JsonObject) parser.parse(resp.getMessage());

        String msg = jsonObj.get("message").getAsString();
        String action = jsonObj.get("action").getAsString();
        
        String flow = "";
        if (action.equals("C")){
            //Respond with USSR response 
            flow = "1";
        }else if (action.equals("S")){
            //Respond with PSSR response and clear the session
            flow = "2";
            sessionMgt.deleteSession(resp.getRequestId());
        }else{
            flow = "2";
            sessionMgt.deleteSession(resp.getRequestId());
        }
        
        String responseString = "<USSDDynMenuResponse>\n" +
            "	<requestId>"+resp.getRequestId()+"</requestId>\n" +
            "	<msisdn>"+resp.getMsisdn()+"</msisdn>\n" +
            "	<starCode>"+resp.getStarCode()+"</starCode>\n" +
            "	<langId>1</langId>\n" +
            "	<encodingScheme>0</encodingScheme>\n" +
            "	<dataSet>\n" +
            "		<param>\n" +
            "			<id>1</id>\n" +
            "		    <value>" + msg+
            "</value>\n" +
            "			<rspFlag>"+flow+"</rspFlag>\n" +
            "			<rspTag>response</rspTag>\n" +
            "			<rspURL>"+env.getProperty("url.ussd.response")+"</rspURL>\n" +
            "			<appendIndex>0</appendIndex>\n" +
            "			<default>1</default>\n" +
            "		</param>\n" +
            "	</dataSet>\n" +
            "	<ErrCode>1</ErrCode>\n" +
            "   <errURL>"+env.getProperty("url.ussd.response")+"</errURL>\n" +
            "	<timeStamp>"+resp.getTimestamp()+"</timeStamp>\n" +
            "</USSDDynMenuResponse> ";
        
        log.error("ussdResponse: "+responseString);
        
        return responseString;
    }
    /*
    public UssdRequest parseUssdDom4j(String req){
        SAXReader reader = new SAXReader();
        try {
            Document document = (Document) reader.read(req);
            Node
            
        } catch (DocumentException ex) {
            Logger.getLogger(UssdUtil.class.getName()).log(Level.SEVERE, null, ex);
        }
    }*/
    
    public String validateMsisdn(String msisdn){
        String newMsisdn;
        //254763100100 || 0763100100
        if(msisdn.length() == 12 && msisdn.startsWith("2")){
            newMsisdn = msisdn;
        }else if(msisdn.length() == 10 && msisdn.startsWith("0")){
            newMsisdn = "254"+msisdn.substring(1, 10);
        }else{
            newMsisdn = null;
        }
        
        return newMsisdn;
    }
}
