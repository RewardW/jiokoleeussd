/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.equitel.jiokolee.pojos;

/**
 *
 * @author Wambayi
 */
public class SessionData {
    public String sessionId;
    public UssdRequest ussdRequest;

    public String getSessionId() {
        return sessionId;
    }

    public void setSessionId(String sessionId) {
        this.sessionId = sessionId;
    }

    public UssdRequest getUssdRequest() {
        return ussdRequest;
    }

    public void setUssdRequest(UssdRequest ussdRequest) {
        this.ussdRequest = ussdRequest;
    }
}
