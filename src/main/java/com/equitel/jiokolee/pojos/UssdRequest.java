/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.equitel.jiokolee.pojos;

import java.util.ArrayList;

/**
 *
 * @author Wambayi
 */
public class UssdRequest {
    String requestId;
    String msisdn;
    String timestamp;
    String starCode;
    String input;
    String message;
    String requestType;
    int menuLevel; //What part of the menu are we on?
    Integer[] denominations;
    int amountSelected;
    ArrayList<String> dataNames;
    ArrayList<String> dataValues;
    String productSelected;
    private int flow; //Continuing session or end

    public String getRequestId() {
        return requestId;
    }

    public void setRequestId(String requestId) {
        this.requestId = requestId;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getStarCode() {
        return starCode;
    }

    public void setStarCode(String starCode) {
        this.starCode = starCode;
    }

    public String getInput() {
        return input;
    }

    public void setInput(String input) {
        this.input = input;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public String getRequestType() {
        return requestType;
    }

    public void setRequestType(String requestType) {
        this.requestType = requestType;
    }

    public int getMenuLevel() {
        return menuLevel;
    }

    public void setMenuLevel(int menuLevel) {
        this.menuLevel = menuLevel;
    }

    public int getFlow() {
        return flow;
    }

    public void setFlow(int flow) {
        this.flow = flow;
    }

    public Integer[] getDenominations() {
        return denominations;
    }

    public void setDenominations(Integer[] denominations) {
        this.denominations = denominations;
    }

    public int getAmountSelected() {
        return amountSelected;
    }

    public void setAmountSelected(int amountSelected) {
        this.amountSelected = amountSelected;
    }

    public ArrayList<String> getDataNames() {
        return dataNames;
    }

    public void setDataNames(ArrayList<String> dataNames) {
        this.dataNames = dataNames;
    }

    public ArrayList<String> getDataValues() {
        return dataValues;
    }

    public void setDataValues(ArrayList<String> dataValues) {
        this.dataValues = dataValues;
    }

    public String getProductSelected() {
        return productSelected;
    }

    public void setProductSelected(String productSelected) {
        this.productSelected = productSelected;
    }

    @Override
    public String toString() {
        return "UssdRequest{" + "requestId=" + requestId + ", msisdn=" + msisdn + ", timestamp=" + timestamp + ", starCode=" + starCode + ", input=" + input + ", message=" + message + ", requestType=" + requestType + ", menuLevel=" + menuLevel + ", denominations=" + denominations + ", amountSelected=" + amountSelected + ", dataNames=" + dataNames + ", dataValues=" + dataValues + ", productSelected=" + productSelected + ", flow=" + flow + '}';
    }
}
