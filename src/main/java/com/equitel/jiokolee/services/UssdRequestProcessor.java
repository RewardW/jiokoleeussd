/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.equitel.jiokolee.services;

import com.equitel.jiokolee.JiokoleeUssdApplication;
import com.equitel.jiokolee.pojos.SessionData;
import com.equitel.jiokolee.pojos.UssdRequest;
import com.equitel.jiokolee.utils.Tools;
import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import java.io.IOException;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Date;
import java.util.Set;
import java.util.concurrent.ExecutionException;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

/**
 *
 * @author Wambayi
 */

@Service
public class UssdRequestProcessor {
    
    static Log log = LogFactory.getLog(UssdRequestProcessor.class.getName());
    
    @Autowired
    Rpc rpc;
    
    @Autowired
    Tools tools;
    
    @Autowired
    Environment env;

    public UssdRequest requestHandler(UssdRequest request){
        
        SessionManagement sessionMgr = JiokoleeUssdApplication.sessionMgr;
        SessionData sessionData = sessionMgr.getSessionData(request.getRequestId());
        
        if(sessionData != null && request.getMsisdn().equals(sessionData.getUssdRequest().getMsisdn())){
            
            log.info("ContinuingSession .............................");
            
            log.info("SessionData: " + sessionData.getUssdRequest().toString());
            
            //Continuing request. Get where it has reached on the menu
            int level = sessionData.getUssdRequest().getMenuLevel();
            String input = tools.extractInput(request.getInput());
            String msisdn = sessionData.getUssdRequest().getMsisdn();
            //if(level==0 && input.equals("9")){ //Help
            if(input.equals("9")){ //Help
                sessionData.getUssdRequest().setMessage(getMenu(sessionData.getUssdRequest(), 30));
                sessionData.getUssdRequest().setMenuLevel(30);
                return sessionData.getUssdRequest();
            }else {
                if(level==10){
                    //Jiokolee Credo: Variations
                    if(input.equals("1")){ //Normal Jiokolee
                        sessionData.getUssdRequest().setMessage(getMenu(sessionData.getUssdRequest(), 11));
                        sessionData.getUssdRequest().setMenuLevel(11);
                        return sessionData.getUssdRequest();
                    }else if(input.equals("2")){ //Auto Jiokolee
                        //String txnId = EsmeSmpp.sessionParam.getSessionId(msisdn).getSessionId();
                        String txnId = sessionData.getUssdRequest().getRequestId();
                        JsonObject subscription = null;
                        try {
                            subscription = rpc.checkSubscription(txnId,sessionData.getUssdRequest().getMsisdn());
                            int amountSubscribed = Integer.parseInt(subscription.get("amountSubscribed").toString());

                            if(amountSubscribed > 0){
                                sessionData.getUssdRequest()
                                        .setMessage(
                                                getMenu(sessionData.getUssdRequest(), 18,Integer.toString(amountSubscribed)));
                                sessionData.getUssdRequest().setMenuLevel(18);
                                return sessionData.getUssdRequest();
                            }else{
                                sessionData.getUssdRequest().setMessage(getMenu(sessionData.getUssdRequest(), 15));
                                sessionData.getUssdRequest().setMenuLevel(15);
                                return sessionData.getUssdRequest();
                            }
                        } catch (IOException ex) {
                            log.error(ex);
                            
                            sessionData.getUssdRequest().setMessage(getMenu(sessionData.getUssdRequest(), 50));
                            return sessionData.getUssdRequest();
                        }
                        
                    }else if(input.equals("0")){ //Back
                        sessionData.getUssdRequest().setMessage(getMenu(sessionData.getUssdRequest(), 0));
                        sessionData.getUssdRequest().setMenuLevel(0);
                        return sessionData.getUssdRequest();
                        
                    }else{ //Wrong option entered
                        sessionData.getUssdRequest().setMessage(getMenu(sessionData.getUssdRequest(), 10,"true"));
                        return sessionData.getUssdRequest();
                    }
                }else if(level==11){ //Select Airtime Denomination
                    //Check if input is valid
                    Integer[] denoms = sessionData.getUssdRequest().getDenominations();
                    for(int d =0; d < denoms.length;d++){
                        log.info("DenomItem: "+d+" | DenomValue:"+denoms[d]);
                    }
                    log.info("SelectDenom: DenomLength:" + denoms.length +" - Input:"+input+" - DenomValue:"+denoms[Integer.parseInt(input)-1]);
                    if(Integer.parseInt(input) > denoms.length){
                        //Denom does not exist 
                        //Wrong input
                        sessionData.getUssdRequest().setMessage(getMenu(sessionData.getUssdRequest(), 11,"true"));
                        return sessionData.getUssdRequest();
                    }else if(input.equals("0")){ //Back
                        sessionData.getUssdRequest().setMessage(getMenu(sessionData.getUssdRequest(), 11));
                        sessionData.getUssdRequest().setMenuLevel(11);
                        return sessionData.getUssdRequest();
                    }else{
                        sessionData.getUssdRequest().setAmountSelected(denoms[Integer.parseInt(input)-1]);
                        sessionData.getUssdRequest().setMenuLevel(level +1);
                        sessionData.getUssdRequest().setMessage(getMenu(sessionData.getUssdRequest(), 12));
                        return sessionData.getUssdRequest();
                    }
                }else if(level == 12){ // Confirm Airtime Request
                    //Check if input is valid
                    if(Integer.parseInt(input) == 1){
                        //Customer selected yes. Send the credit request
                        int amount = sessionData.getUssdRequest().getAmountSelected();
                        String txnId = tools.generateId();
                        JsonObject lendAirtime = null;
                        try {
                            lendAirtime = rpc.callAirtimeLending(txnId, msisdn, amount);
                            int status = lendAirtime.get("responseCode").getAsInt();
                            
                            if(status == 200){
                                int amountCredited = lendAirtime.get("amountBorrowed").getAsInt();
                                String repaymentDate = parseDate(lendAirtime.get("repaymentDate").getAsString());
                                log.debug("LendResponse: "+ status +" | "+ amountCredited);
                                //Successful message
                                sessionData.getUssdRequest().setMessage(getMenu(sessionData.getUssdRequest(), level+1, Integer.toString(amountCredited),repaymentDate));
                                return sessionData.getUssdRequest();
                            }else{
                                //Error message
                                sessionData.getUssdRequest().setMessage(getMenu(sessionData.getUssdRequest(), 50));
                                return sessionData.getUssdRequest();
                            }
                        } catch (IOException ex) {
                            log.error(ex);
                            sessionData.getUssdRequest().setMessage(getMenu(sessionData.getUssdRequest(), 50));
                            return sessionData.getUssdRequest();
                        }

                    }else if(Integer.parseInt(input) == 2){
                        //Cancel credit request
                        sessionData.getUssdRequest().setMenuLevel(11);
                        sessionData.getUssdRequest().setMessage(getMenu(sessionData.getUssdRequest(), 11));
                        return sessionData.getUssdRequest();
                    }else{
                        //Wrong input
                        sessionData.getUssdRequest().setMessage(getMenu(sessionData.getUssdRequest(), 12,"true"));
                        return sessionData.getUssdRequest();
                    }
                }
                /*
                else if(level==15){ //Select Subscription AMount
                    //Check if input is valid
                    int[] denoms = EsmeSmpp.sessionParam.getDenominations(msisdn);
                    if(Integer.parseInt(input) > denoms.length){
                        //Denom does not exist
                        text = getMenu(msisdn, 15,"true");
                        return text;
                    }else{
                        EsmeSmpp.sessionParam.updateDenomSelected(msisdn, denoms[Integer.parseInt(input)-1]);
                        text = getMenu(msisdn, 16);
                        EsmeSmpp.sessionParam.updateSession(msisdn, level + 1);
                        return text;
                    }
                    
                }else if(level == 16){ //Confirm Subscription Amount
                    //Check if input is valid
                    if(Integer.parseInt(input) == 1){
                        //Customer selected yes. Send the credit request
                        int amount = EsmeSmpp.sessionParam.getDenomSelected(msisdn);
                        String txnId = EsmeSmpp.sessionParam.getSessionId(msisdn).getSessionId();
                        JsonObject subscribeAirtime = null;
                        try {
                            subscribeAirtime = rpc.addSubscription(txnId, msisdn, amount);
                            int status = subscribeAirtime.get("responseCode").getAsInt();
                            
                            if(status == 200){
                                int amountSubscribed = subscribeAirtime.get("amountSubscribed").getAsInt();
                                logger.debug("subscriptionResponse: "+ status +" | "+ amountSubscribed);
                                //Successful message
                                text = getMenu(msisdn, level+1, Integer.toString(amountSubscribed));
                                return text;
                            }else{
                                //Error message
                                text = getMenu(msisdn, 50);
                                return text;
                            }
                        } catch (IOException ex) {
                            logger.error(ex);
                            text = getMenu(msisdn, 50);
                            return text;
                        }

                    }else if(Integer.parseInt(input) == 2){
                        EsmeSmpp.sessionParam.updateSession(msisdn, 0);
                        text = getMenu(msisdn, 0);
                        return text;
                    }else{
                        text = getMenu(msisdn, 16,"true");
                        return text;
                    }
                }else if(level==18){ //Already Subscribed - Delete process
                    //Check if input is valid
                    if(Integer.parseInt(input) == 1){
                        //Customer selected yes. Send the credit request
                        String txnId = EsmeSmpp.sessionParam.getSessionId(msisdn).getSessionId();
                        JsonObject deleteSubscription = null;
                        try {
                            deleteSubscription = rpc.deleteSubscription(txnId, msisdn);
                            int status = deleteSubscription.get("responseCode").getAsInt();
                            
                            if(status == 200){
                                //Successful message
                                text = getMenu(msisdn, level+1);
                                return text;
                            }else{
                                //Error message
                                text = getMenu(msisdn, 50);
                                return text;
                            }
                        } catch (IOException ex) {
                            logger.error(ex);
                            text = getMenu(msisdn, 50);
                            return text;
                        }

                    }else{
                        EsmeSmpp.sessionParam.updateSession(msisdn, 0);
                        text = getMenu(msisdn, 0);
                        return text;
                    }
                }else if(level==20){ //Select Data Denomination
                    //Check if input is valid
                    String[] denomNames = EsmeSmpp.sessionParam.getDataDenomNames(msisdn);
                    String[] denomValues = EsmeSmpp.sessionParam.getDataDenomValues(msisdn);
                    
                    if(Integer.parseInt(input) > denomValues.length){
                        //Denom does not exist
                        text = getMenu(msisdn, 20,"true");
                        return text;
                    }else{
                        logger.debug("Selected DenomName:" + denomNames[Integer.parseInt(input)-1]);
                        logger.debug("Selected DenomValue:" + denomValues[Integer.parseInt(input)-1]);
                        EsmeSmpp.sessionParam.updateSelectedDataDenom(msisdn, Integer.parseInt(denomValues[Integer.parseInt(input)-1]),denomNames[Integer.parseInt(input)-1]);
                        text = getMenu(msisdn, 21);
                        EsmeSmpp.sessionParam.updateSession(msisdn, level + 1);
                        return text;
                    }
                }else if(level==21){ //Confirm Data Denomination
                    //Check if input is valid
                    if(Integer.parseInt(input) == 1){
                        //Customer selected yes. Send the credit request
                        int amount = EsmeSmpp.sessionParam.getDenomSelected(msisdn);
                        String txnId = EsmeSmpp.sessionParam.getSessionId(msisdn).getSessionId();
                        JsonObject lendAirtime = null;
                        try {
                            lendAirtime = rpc.callDataLending(txnId, msisdn, amount);
                            int status = lendAirtime.get("responseCode").getAsInt();
                            
                            if(status == 200){
                                String repaymentDate = parseDate(lendAirtime.get("repaymentDate").getAsString());
                                logger.debug("LendResponse: "+ status +" | "+ amount);
                                //Successful message
                                text = getMenu(msisdn, level+1,repaymentDate);
                                return text;
                            }else{
                                //Error message
                                text = getMenu(msisdn, 50);
                                return text;
                            }
                        } catch (IOException ex) {
                            logger.error(ex);
                            text = getMenu(msisdn, 50);
                            return text;
                        }

                    }else if(Integer.parseInt(input) == 2){
                        EsmeSmpp.sessionParam.updateSession(msisdn, 0);
                        text = getMenu(msisdn, 0);
                        return text;
                    }else{
                        text = getMenu(msisdn, 21, "true");
                        return text;
                    }
                }*/
                else if(level==30){//Help
                    if (Integer.parseInt(input) == 1){
                        sessionData.getUssdRequest().setMenuLevel(31);
                        sessionData.getUssdRequest().setMessage(getMenu(sessionData.getUssdRequest(), 31));
                        return sessionData.getUssdRequest();
                    }else if(Integer.parseInt(input) == 2){
                        sessionData.getUssdRequest().setMenuLevel(32);
                        sessionData.getUssdRequest().setMessage(getMenu(sessionData.getUssdRequest(), 32));
                        return sessionData.getUssdRequest();
                    }else if(input.equals("0")){ //back
                        sessionData.getUssdRequest().setMessage(getMenu(sessionData.getUssdRequest(), 11));
                        sessionData.getUssdRequest().setMenuLevel(11);
                        return sessionData.getUssdRequest();
                    }else{
                        sessionData.getUssdRequest().setMessage(getMenu(sessionData.getUssdRequest(),30,"true"));
                        return sessionData.getUssdRequest();
                    }
                }else if(level==31){
                    if (input.equals("0")){//Back
                        sessionData.getUssdRequest().setMenuLevel(11);
                        sessionData.getUssdRequest().setMessage(getMenu(sessionData.getUssdRequest(), 11));
                        return sessionData.getUssdRequest();
                    }else{
                        sessionData.getUssdRequest().setMessage(getMenu(sessionData.getUssdRequest(),31,"true"));
                        return sessionData.getUssdRequest();
                    }
                }else if(level==32){
                    if (input.equals("0")){//Back
                        sessionData.getUssdRequest().setMenuLevel(11);
                        sessionData.getUssdRequest().setMessage(getMenu(sessionData.getUssdRequest(), 11));
                        return sessionData.getUssdRequest();
                    }else{
                        sessionData.getUssdRequest().setMessage(getMenu(sessionData.getUssdRequest(),32,"true"));
                        return sessionData.getUssdRequest();
                    }
                }else{
                    //text = getMenu(msisdn,level + Integer.parseInt(input));
                    //EsmeSmpp.sessionParam.updateSession(msisdn, level + Integer.parseInt(input));
                    sessionData.getUssdRequest().setMessage(getMenu(sessionData.getUssdRequest(), level,"true"));
                }
            }
        }else{
            
            log.info("NewSession .............................");
            
            sessionData = new SessionData();
            
            try {
                //Main Menu
                JsonObject qualificationCheck = null;
                qualificationCheck = rpc.getCustomerProfile(request.getMsisdn());
                
                log.info("QualificationCheck:"+qualificationCheck);

                int creditScore = Integer.parseInt(qualificationCheck.get("creditScore").toString());
                int debtBalance = Integer.parseInt(qualificationCheck.get("debtBalance").toString());
                Float airtimeBalance = new Float(0);
                if(creditScore > 0){
                    airtimeBalance = Float.parseFloat(qualificationCheck.get("airtimeBalance").toString());
                }
                String suspensionDate = qualificationCheck.get("suspensionEndDate").toString();
                Boolean suspended = isSuspended(suspensionDate);
                
                //Get list of Airtime denminations
                JsonArray denomsList = (JsonArray) qualificationCheck.get("denominations");
                
                Integer[] denoms = new Integer[denomsList.size()];
                for(int i=0;i<denomsList.size();i++){
                    denoms[i] = denomsList.get(i).getAsInt();
                }
                
                //Collections.reverse(denoms);
                
                Arrays.sort(denoms, Collections.reverseOrder());
                
                //Get list of Data denominations
                //JsonArray denomsListData = qualificationCheck.get("dataDenominations").getAsJsonArray();
                //Set<String> denomsData = denomsListData.keySet();
                ArrayList<String> dataDenomNames = new ArrayList<String>();
                ArrayList<String> dataDenomValues = new ArrayList<String>();
                /*
                for(int i = 0; i < denomsListData.size();i++){
                    dataDenomValues.add(denomsListData.);
                    dataDenomNames.add(denomsListData.get(dN).getAsString());
                }
                
                for(String dN : denomsData){
                    dataDenomValues.add(dN);
                    dataDenomNames.add(denomsListData.get(dN).getAsString());
                }*/
                log.info("requestParams{creditScore:"+creditScore+",debtBalance:"
                        +debtBalance+",suspended:"+suspended+",airtimeBalance:"+airtimeBalance+"}");
                if (creditScore == 0){ //Not Qualified
                    request.setMessage(getMenu(request, 1));
                    request.setMenuLevel(1);
                    //EsmeSmpp.sessionParam.updateSession(msisdn, 1);
                    //return sessionData.getUssdRequest();
                }else if (debtBalance > 0){ //Debtor
                    request.setMessage(getMenu(request, 2,Integer.toString(creditScore),Integer.toString(debtBalance)));
                    request.setMenuLevel(2);
                    //return sessionData.getUssdRequest();
                    //}else if(airtimeBalance > 5){ //More than the airtime balance required
                    //    } else if (airtimeBalance.floatValue() > 5.0F) {
                    //        text = getMenu(msisdn, 4);
                    //        EsmeSmpp.sessionParam.updateSession(msisdn, 4);
                    //        return text;
                    //}else if(suspended == true){ //Suspended for late payment
                } else if (suspended.booleanValue() == true) {
                    request.setMessage(getMenu(request, 3,parseDate3(suspensionDate)));
                    request.setMenuLevel(3);
                    //return sessionData.getUssdRequest();
                    //} else if ((creditScore > 0) && (debtBalance == 0) && (!suspended.booleanValue()) && (airtimeBalance.floatValue() < 5.0F)) {
                } else if ((creditScore > 0) && (debtBalance == 0) && (!suspended.booleanValue())) {
                    request.setDenominations(denoms);
                    request.setDataNames(dataDenomNames);
                    request.setDataValues(dataDenomValues);
                    request.setMessage(getMenu(request,11));
                    request.setMenuLevel(11);
                    //return sessionData.getUssdRequest();
                    
                } else{
                    request.setMessage(getMenu(request,50));
                    //return sessionData.getUssdRequest();
                }
            } catch(NullPointerException exception){
                Logger.getLogger(UssdRequestProcessor.class.getName()).log(Level.SEVERE, null, exception);
                request.setMessage(getMenu(request, 50));
                //return sessionData.getUssdRequest();
            }catch (IOException ex) {
                Logger.getLogger(UssdRequestProcessor.class.getName()).log(Level.SEVERE, null, ex);
                request.setMessage(getMenu(request, 50));
                //return sessionData.getUssdRequest();
            } catch (InterruptedException ex) {
                Logger.getLogger(UssdRequestProcessor.class.getName()).log(Level.SEVERE, null, ex);
                request.setMessage(getMenu(request, 50));
                //return sessionData.getUssdRequest();
            } catch (ExecutionException ex) {
                Logger.getLogger(UssdRequestProcessor.class.getName()).log(Level.SEVERE, null, ex);
                request.setMessage(getMenu(request, 50));
                //return sessionData.getUssdRequest();
            }catch (Exception ex){
                Logger.getLogger(UssdRequestProcessor.class.getName()).log(Level.SEVERE, null, ex);
                request.setMessage(getMenu(request, 50));
            }
        }
        
        sessionData.setUssdRequest(request);
        sessionMgr.newSession(request.getRequestId(), sessionData);
        
        return sessionData.getUssdRequest();
    }
    
    public String getMenu(UssdRequest req,int menuLevel, String ... values){
        
        String menuText  = null;
        String menuAction = null;
        String wrongInput = "";
        //Incase of wrong input
        
        if(values.length > 0){
            if(values[0].equals("true")){
                wrongInput = "Wrong input\n";
            }
        }
       
        switch(menuLevel){
            case 0: //Main Menu
                //menuText = "Welcome to Jiokolee\nSelect\n1. Jiokolee Credo\n2. Jiokolee Data\n3. Help";
                menuText = env.getProperty("msg-ussd-main-menu");
                menuAction = "C";
                break;
            case 1: //Not Qualified
                //menuText = "Dear customer, you are not qualified for this service. You need to be active for atleast 3 months and use at least KES 20 monthly.\n9. Help";
                menuText = env.getProperty("msg-ussd-not-qualified");
                menuAction = "C";
                //rpc.sendSMS(msisdn, menuText);
                break;
            case 2: //Debtor
                //menuText = String.format("Dear customer, you still owe Jiokolee KES%s. Please topup to repay.\n9. Help",values[1]);
                menuText = String.format(env.getProperty("msg-ussd-has-debt"),values[1]);
                menuAction = "C";
                //rpc.sendSMS(msisdn, menuText);
                break;
            case 3: //Suspended
                //menuText = String.format("Dear customer, you have been suspended from the service till %s for late repayment.\n9. Help",values[0]);
                menuText = String.format(env.getProperty("msg-ussd-suspended"),values[0]);
                menuAction = "C";
                //rpc.sendSMS(msisdn, menuText);
                break;
            case 4: //More than minimum balance
                menuText = "Dear customer, you need to have Airtime less than Kes 5 to access Jiokolee Credo.\n9. Help";
                menuAction = "C";
                //rpc.sendSMS(msisdn, menuText);
                break;
            case 10: //Airtime Menu
                menuText = "Select\n1. Jiokolee Airtime\n2. Auto Jiokolee Airtime\n0. Back";
                menuAction = "C";
                break;
            case 11: //Menu Airtime
                Integer[] denoms = req.getDenominations();
                
                String denomsList = "";
                for(int i = 0;i<denoms.length;i++){
                    int menuNumber = i +1;
                    log.info("Denom : index:" +i+ " | value:" +denoms[i]);
                    denomsList+= menuNumber +". Ksh"+denoms[i]+"\n";
                }
                menuText = String.format(env.getProperty("msg-ussd-airtime-menu"),denomsList);
                log.info("generatedMenu" + menuText);
                menuAction = "C";
                break;
            case 12: //Confirm Airtime
                int amountSelected = req.getAmountSelected();
                //menuText = String.format("You will be credited with KES%s airtime. A service fee of 10 percent will be charged.\nSelect\n1. Yes\n2. No",amountSelected);
                menuText = String.format(env.getProperty("msg-ussd-airtime-confirm"),amountSelected);
                menuAction = "C";
                break;
            case 13: //Success Aitime
                //menuText = String.format("You have been successfully credited with KES%s Airtime. Please recharge by %s to repay your Jiokolee Credo.",values[0],values[1]);
                menuText = String.format(env.getProperty("msg-ussd-airtime-lend"),values[0],values[1]);
                menuAction = "S";
                break;
            case 15: //Auto Jiokolee Choise menu
                Integer[] autoDenoms = req.getDenominations();
                String denomsListAuto = "";
                for(int i = 0;i<autoDenoms.length;i++){
                    int menuNumber = i +1;
                    denomsListAuto+= menuNumber +". Ksh"+autoDenoms[i]+"\n";
                }
                menuText = String.format("Auto Jiokolee Credo\nSelect\n%s0. Back",denomsListAuto);
                log.info("generatedMenu" + menuText);
                menuAction = "C";
                break;
            case 16: //Auto Jiokolee Confirmation
                int subscAmountSelected = req.getAmountSelected();
                menuText = String.format("You will be credited with KES%s airtime everytime your balance runs below KES 5. A service fee of 10 percent will be charged.\nSelect\n1. Yes\n2. No",subscAmountSelected);
                menuAction = "C";
                break;
            case 17: //Auto Jiokolee Success
                menuText = String.format("You have been subscribed to Jiokolee Auto. You will be credited with KES%s when your balance runs below KES 5.",values[0]);
                menuAction = "S";
                break;
            case 18: //Already Subscribed
                menuText = String.format("You are subscribed to Jiokolee Auto KES%s Airtime. \nSelect\n1. Stop\n2. Back",values[0]);
                menuAction = "C";
                break;
            case 19: //Stop Subscription Successful
                menuText = "You have successfully cancelled your Jiokolee Auto Airtime. Dial *740# for more options";
                menuAction = "S";
                break;
            /*case 20: //Data Menu 
                String[] denomNames = req.getDataNames();
                String[] denomValues = req.getDataValues();
                
                String dataDenomsList = "";
                for(int i = 0;i<denomNames.length;i++){
                    int menuNumber = i +1;
                    dataDenomsList+= menuNumber +". "+ denomNames[i] +" @ Ksh."+denomValues[i]+"\n";
                }
                menuText = String.format("Jiokolee Data\nSelect\n%s0. Back",dataDenomsList);
                menuAction = "C";
                break;*/
            case 21: //Confirm Data
                int dataAmount= req.getAmountSelected();
                String productName = req.getProductSelected();
                menuText = String.format("You will be credited with %s @ KES%s.  A service fee of 10 percent will be charged.\nSelect\n1. Yes\n2. No",
                        productName,dataAmount);
                menuAction = "C";
                break;
            case 22:
                String productNameCredited = req.getProductSelected();
                menuText = String.format("You have been successfully credited with %s. Please recharge by %s to repay you Jiokolee Credo.",productNameCredited,values[0]);
                menuAction = "S";
                break;
            case 30: //About
                menuText = env.getProperty("msg-ussd-help-menu");
                menuAction = "C";
                break;
            case 31: //Eligibility
                menuText = env.getProperty("msg-ussd-help-eligibility");
                menuAction = "S";
                break;
            case 32: //Repayment
                menuText = env.getProperty("msg-ussd-help-repayment");
                menuAction = "S";
                break;
            default:
                menuText = env.getProperty("msg-ussd-processing-error");
                menuAction = "S";
                break;
        }
        
        JsonObject resp = new JsonObject();
        resp.addProperty("message", wrongInput + menuText);
        resp.addProperty("action", menuAction);
        return resp.toString();
    }
    
    private String parseDate(String date){
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy hh:mm:ss.SSS");
        DateFormat dfResponse = new SimpleDateFormat("dd/MM/yyyy");
        String respRepaymentDate = "";
        try {
            
            Date repaymentDt = df.parse(date);
            respRepaymentDate = dfResponse.format(repaymentDt);
            
        } catch (ParseException ex) {
            log.error(ex);
        }
        return respRepaymentDate;
    }
    
    private String parseDate2(String date){
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        DateFormat dfResponse = new SimpleDateFormat("dd/MM/yyyy");
        String respRepaymentDate = "";
        try {
            
            Date repaymentDt = df.parse(date);
            respRepaymentDate = dfResponse.format(repaymentDt);
            
        } catch (ParseException ex) {
            log.error(ex);
        }
        return respRepaymentDate;
    }
    
    private String parseDate3(String date){
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        DateFormat dfResponse = new SimpleDateFormat("dd/MM/yyyy");
        String respRepaymentDate = "";
        
        Date repaymentDt = new Date(Long.parseLong(date));
        respRepaymentDate = dfResponse.format(repaymentDt);
        
        return respRepaymentDate;
    }
    
    private Date getDate(Long date){
        DateFormat df = new SimpleDateFormat("dd-MM-yyyy");
        Date respRepaymentDate = null;
        
        respRepaymentDate = new Date(date);
        //respRepaymentDate = df.parse(date);
        
        return respRepaymentDate;
    }
    
    private Boolean isSuspended(String suspensionDate){
        
        Boolean resp = null;
        
        if(suspensionDate == null || suspensionDate.equals("null")){
            resp = false;            
        }else{
            Date now = new Date();
            resp = getDate(Long.parseLong(suspensionDate)).after(now);
        }
        
        return resp;        
    }
}
