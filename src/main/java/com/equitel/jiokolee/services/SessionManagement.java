/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.equitel.jiokolee.services;

import com.equitel.jiokolee.pojos.SessionData;
import java.util.Hashtable;
import java.util.Map;

/**
 *
 * @author Wambayi
 */
public class SessionManagement {
    Map<String,SessionData> sessionParams = null;

    public SessionManagement() {
        this.sessionParams = new Hashtable();
    }
    
    public void newSession(String requestId,SessionData sessionData){
        this.sessionParams.put(requestId, sessionData);
    } 
    
    public void updateSession(String requestId,SessionData sessionData){
        this.sessionParams.replace(requestId, sessionData);
    }
    
    public SessionData getSessionData(String msisdn){
        SessionData sessionData = this.sessionParams.get(msisdn);
        return sessionData;        
    }
    
    public void deleteSession(String requestId){
        this.sessionParams.remove(requestId);
    }

    @Override
    public String toString() {
        return "SessionManagement{" + "sessionParams=" + sessionParams + '}';
    }
}
