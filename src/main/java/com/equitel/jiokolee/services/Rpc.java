/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.equitel.jiokolee.services;

import com.equitel.jiokolee.pojos.ApiResponse;
import com.equitel.jiokolee.utils.HttpUtils;
import com.equitel.jiokolee.utils.TrustModifier;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import java.io.BufferedReader;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketTimeoutException;
import java.net.URL;
import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutionException;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.logging.log4j.LogManager;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Service;

/**
 *
 * @author Wambayi
 */
@Service
public class Rpc {
    ExecutorService executorService = Executors.newFixedThreadPool(10);
    
    @Autowired
    Environment env;
    
    @Autowired
    HttpUtils httpUtils;
    
    static Log logger = LogFactory.getLog(Rpc.class.getName());
    
    public JsonObject callAirtimeLending(String requestId, String msisdn, int amount) throws IOException{
        String url = env.getProperty("as-url") + "/advancing/airtime";
        logger.info("callLendingURL: " + url);
        JsonObject lendBody = new JsonObject();
        lendBody.addProperty("trnId", Long.parseLong(requestId));
        lendBody.addProperty("msisdn", msisdn);
        lendBody.addProperty("amountBorrowed", amount);
        
        logger.info("callLending: " + lendBody.toString());
        JsonObject response = new JsonObject();
        response = rpcCall(url, lendBody);
        //return response.toString();
        return response;
    }
    
    public JsonObject callDataLending(String requestId, String msisdn, int amount) throws IOException{
        String url = env.getProperty("as-url") + "/advancing/data";
        logger.info("callLendingURL: " + url);
        JsonObject lendBody = new JsonObject();
        lendBody.addProperty("trnId",Integer.parseInt(requestId));
        lendBody.addProperty("msisdn", msisdn);
        lendBody.addProperty("amount", amount);
        
        logger.info("callLending: " + lendBody.toString());
        JsonObject response = new JsonObject();
        response = rpcCall(url, lendBody);
        //return response.toString();
        return response;
    }
    
    public JsonObject getCustomerProfile(String msisdn) throws IOException, InterruptedException, ExecutionException{
        
        String url = env.getProperty("as-url") + "/customers/qualificationCheck";
        logger.info("getCustomerProfileURL: " + url);
        JsonObject customer = new JsonObject();
        customer.addProperty("msisdn", msisdn);
        
        logger.info("getCustomerProfile: " + customer.toString());
        JsonObject response = new JsonObject();
        
        //Future future = executorService.submit(new CallMiddleware(url,customer){});
        
        //response = (JsonObject) future.get();
        response = rpcCall(url, customer);
        return response;
    }
    
    public JsonObject addSubscription(String requestId,String msisdn, int amount) throws IOException{
        String url = env.getProperty("as-url") + "/subscribe/add";
        logger.info("addSubscriptionURL: " + url);
        JsonObject lendBody = new JsonObject();
        lendBody.addProperty("requestId",Integer.parseInt(requestId));
        lendBody.addProperty("msisdn", msisdn);
        lendBody.addProperty("subscriptionAmount", amount);
        lendBody.addProperty("requestType", "add");
        
        logger.info("addSubscription: " + lendBody.toString());
        JsonObject response = new JsonObject();
        response = rpcCall(url, lendBody);
        return response;
    }
    
    public JsonObject deleteSubscription(String txnId,String msisdn) throws IOException{
        String url = env.getProperty("as-url") + "/subscribe/delete";
        logger.info("deleteSubscriptionURL: " + url);
        JsonObject customer = new JsonObject();
        customer.addProperty("msisdn", msisdn);
        customer.addProperty("requestId",Integer.parseInt(txnId));
        customer.addProperty("subscriptionAmount", 0);
        customer.addProperty("requestType", "delete");
        
        logger.info("deleteSubscription: " + customer.toString());
        JsonObject response = new JsonObject();
        response = rpcCall(url, customer);
        return response;
    }
    
    public JsonObject checkSubscription(String txnId,String msisdn) throws IOException{
        String url = env.getProperty("as-url") + "/subscribe/check";
        logger.info("checkSubscriptionURL: " + url);
        JsonObject customer = new JsonObject();
        customer.addProperty("msisdn", msisdn);
        customer.addProperty("requestId",Integer.parseInt(txnId));
        customer.addProperty("subscriptionAmount", 0);
        customer.addProperty("requestType", "check");
        
        logger.info("checkSubscription: " + customer.toString());
        JsonObject response = new JsonObject();
        response = rpcCall(url, customer);
        return response;        
    }
    
    
    public JsonObject rpcCall(String url,JsonObject body){
        
        JsonObject jsonResponse = null;
        
        try {
            ApiResponse resp = httpUtils.callApi(url, body.toString(), "application/json");
            
            if(resp.getResponseCode()==200){
                
                JsonParser parser = new JsonParser();
                jsonResponse = (JsonObject) parser.parse(resp.getResponseBody());
            }else{
                JsonObject failureResp = new JsonObject();
                failureResp.addProperty("statusText", "Failed");
                failureResp.addProperty("amountBorrowed", 0);
                jsonResponse = failureResp;
            }
        } catch (InterruptedException ex) {
            Logger.getLogger(Rpc.class.getName()).log(Level.SEVERE, null, ex);
            logger.info("Error: "+ex);
            JsonObject resp = new JsonObject();
            resp.addProperty("statusText", "Failed");
            resp.addProperty("amountBorrowed", 0);
            jsonResponse = resp;
        } catch (ExecutionException ex) {
            Logger.getLogger(Rpc.class.getName()).log(Level.SEVERE, null, ex);
            logger.info("Error: "+ex);
            JsonObject resp = new JsonObject();
            resp.addProperty("statusText", "Failed");
            resp.addProperty("amountBorrowed", 0);
            jsonResponse = resp;
        }
        
        return jsonResponse;
    }
    
    public JsonObject rpcCallOld(String url,JsonObject body) throws MalformedURLException, IOException{
        
        JsonParser parser = new JsonParser();
        JsonObject jsonResponse = null;
        
        URL appURL;
        appURL = new URL(url);
        HttpURLConnection conn =  (HttpURLConnection) appURL.openConnection();        
        try{
            TrustModifier.relaxHostChecking(conn);
            conn.setRequestMethod("POST");
            conn.setConnectTimeout(Integer.parseInt(env.getProperty("as-connect-timeout")));
            conn.setRequestProperty("Content-Type","application/json");
            conn.setReadTimeout(Integer.parseInt(env.getProperty("as-read-timeout")));
            conn.setDoOutput(true);
            
            OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
            wr.write(body.toString());
            wr.flush();

            BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
            String inputLine;
            StringBuffer response =  new StringBuffer();

            while((inputLine = in.readLine()) != null){
                response.append(inputLine);
            }
            in.close();

            logger.debug(response.toString());

            jsonResponse = (JsonObject) parser.parse(response.toString());
            
            logger.debug("As Response: "+ jsonResponse.toString());
        }catch(SocketTimeoutException e){
            logger.error(e.getMessage());
            JsonObject resp = new JsonObject();
            resp.addProperty("statusText", "Failed");
            resp.addProperty("amountBorrowed", 0);
            jsonResponse = resp;
        }catch (IOException e){
            logger.error(e.getMessage());
            JsonObject resp = new JsonObject();
            resp.addProperty("statusText", "Failed");
            resp.addProperty("amountBorrowed", 0);
            jsonResponse = resp;
        } catch (KeyManagementException ex) {
            logger.error(ex);
        } catch (NoSuchAlgorithmException ex) {
            logger.error(ex);
        } catch (KeyStoreException ex) {
            logger.error(ex);
        }
        
        return jsonResponse;
    }
    
    public void sendSMS(String msisdn, String message) {

		StringBuffer response = new StringBuffer();
		String result = "An error occurred";
		try {
			String xml = "<?xml version=\"1.0\" encoding=\"UTF-8\"?>"
					+ "<soap:Envelope xmlns:soap=\"http://schemas.xmlsoap.org/soap/envelope/\">" + "    <soap:Header/>"
					+ "    <soap:Body>" + "        <ns1:send xmlns:ns1=\"http://service.gateway/\">"
					+ "            <smsNo>" + msisdn + "</smsNo>" + "            <smsText>" + message + "</smsText>"
					+ "            <smsSource>diaspora</smsSource>" + "        </ns1:send>" + "    </soap:Body>"
					+ "</soap:Envelope>";
			URL url = new URL(env.getProperty("sms-notification-url"));
			HttpURLConnection conn = (HttpURLConnection) url.openConnection();
			conn.setDoOutput(true);
			conn.setRequestMethod("POST");
			conn.setRequestProperty("Content-Type", "text/xml");

			// Send post request
			conn.setDoOutput(true);
			DataOutputStream wr = new DataOutputStream(conn.getOutputStream());
			wr.writeBytes(xml);
			wr.flush();
			wr.close();

			int responseCode = conn.getResponseCode();
			logger.debug("SMSRequest: "+responseCode+"|"+xml);

			BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
			String inputLine;

			while ((inputLine = in.readLine()) != null) {
				response.append(inputLine);
			}
			in.close();
			logger.debug("SMSResponse"+response.toString());
			result = response.toString();
			conn.disconnect();
		} catch (Exception ex) {
			logger.debug("SMSError: " + ex);
			result = ex.toString();
		}
		return;
	}
    
    private class CallMiddleware implements Callable<JsonObject>{
        
        String url;
        JsonObject body;

        public CallMiddleware(String url, JsonObject body) {
            this.url = url;
            this.body = body;
        }

        @Override
        public JsonObject call() throws Exception {
            JsonParser parser = new JsonParser();
            JsonObject jsonResponse = null;

            URL appURL;
            appURL = new URL(url);
            HttpURLConnection conn =  (HttpURLConnection) appURL.openConnection();        
            try{
                TrustModifier.relaxHostChecking(conn);
                conn.setRequestMethod("POST");
                conn.setConnectTimeout(Integer.parseInt(env.getProperty("as-connect-timeout")));
                conn.setRequestProperty("Content-Type","application/json");
                conn.setReadTimeout(Integer.parseInt(env.getProperty("as-read-timeout")));
                conn.setDoOutput(true);

                OutputStreamWriter wr = new OutputStreamWriter(conn.getOutputStream());
                wr.write(body.toString());
                wr.flush();

                BufferedReader in = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                String inputLine;
                StringBuffer response =  new StringBuffer();

                while((inputLine = in.readLine()) != null){
                    response.append(inputLine);
                }
                in.close();

                logger.debug(response.toString());

                jsonResponse = (JsonObject) parser.parse(response.toString());

                logger.debug("As Response: "+ jsonResponse.toString());
            }catch(SocketTimeoutException e){
                logger.error(e.getMessage());
                JsonObject resp = new JsonObject();
                resp.addProperty("statusText", "Failed");
                resp.addProperty("amountBorrowed", 0);
                jsonResponse = resp;
            }catch (IOException e){
                logger.error(e.getMessage());
                JsonObject resp = new JsonObject();
                resp.addProperty("statusText", "Failed");
                resp.addProperty("amountBorrowed", 0);
                jsonResponse = resp;
            } catch (KeyManagementException ex) {
                logger.error(ex);
            } catch (NoSuchAlgorithmException ex) {
                logger.error(ex);
            } catch (KeyStoreException ex) {
                logger.error(ex);
            }

            return jsonResponse;
        }
        
    }
}
