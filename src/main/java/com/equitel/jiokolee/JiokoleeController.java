/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.equitel.jiokolee;

import com.equitel.jiokolee.pojos.UssdRequest;
import com.equitel.jiokolee.services.UssdRequestProcessor;
import com.equitel.jiokolee.utils.UssdUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Wambayi
 */
@RestController
@RequestMapping("/jiokolee")
public class JiokoleeController {
    @Autowired
    UssdUtils ussdUtils;

    @Autowired
    UssdRequestProcessor ussdRequestProcessor;

    static Log log = LogFactory.getLog(JiokoleeController.class.getName());

    @RequestMapping(value = "/ussd", method = RequestMethod.POST, consumes = {"application/xml","text/html" }, 
            produces = { "application/xml","text/html" })
    public String ussd(@RequestBody String req){
        
        log.info("RawRequest: "+req);
        
        UssdRequest request = ussdUtils.parseUssdReq(req);
    
        log.info("Params: "+request.toString());

        return ussdUtils.ussdResponse(ussdRequestProcessor.requestHandler(request));
    }
}
